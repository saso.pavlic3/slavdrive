/**
* initGoogle function loads array of objects. each object is consisted of:
*    id
*    date
*    lng
*    lat
 * lng and lat are used to make google.maps.LatLng object which holds all the coordinates.
 * every  google.maps.LatLng object is saved in locations array.
 * calculate the total distance and total duration between each point.
 * Result is shown on the dashboard.
 * 
 */
        function initGoogle() {
            
            //map points
            var objects = getObjects();
            var locations = [];
            for (var i = 0; i < objects.length; i++) {
                var object = objects[i];
                var coordinate = new google.maps.LatLng(Number(object.lat), Number(object.lng));
            
                locations.push(coordinate);
            }
            


            //calculate the distance between each point
            for (var i = 1; i < locations.length; i++) {
                var originPoint = locations[i - 1];
                var destinationPoint = locations[i];
                calculate(originPoint, destinationPoint);
            }





            //calculate the distance and duration between each point
            function calculate(originPoint, destinationPoint) {
                var service = new google.maps.DistanceMatrixService();
                distance = 0;
                duration = 0;

                service.getDistanceMatrix(
                    {
                        origins: [originPoint],
                        destinations: [destinationPoint],
                        travelMode: 'DRIVING',
                        avoidHighways: false,
                        avoidTolls: false,
                    }, callback);

                function callback(response, status) {
                    if (status == 'OK') {
                        var origins = response.originAddresses;
                        var destinations = response.destinationAddresses;

                        for (var i = 0; i < origins.length; i++) {
                            var results = response.rows[i].elements;
                            for (var j = 0; j < results.length; j++) {
                                var element = results[j];
                                distance += element.distance.value / 1000;
                                duration += element.duration.value;
                                
                                var from = origins[i];
                                var to = destinations[j];

                            }
                        
                                $('#distance').text(Math.round(distance * 100) / 100 + "km"); //show total driven distance
                                $('#acceleration').text(Math.round(duration/3600) + "h" + Math.round(duration%60) + "min"); //show total driven distance
                        }
                    }
                }
            }




        }
        //calculates elevation between two coordinates
        function getElevation() {
            var elevator = new google.maps.ElevationService;

            elevator.getElevationForLocations({
                "locations": [
                    {
                        "lat": 46.5547,
                        "lng": 15.6459
                    }
                ]
            }, function (results, status) {



                if (status === 'OK') {
                    // Retrieve the first result
                    if (results[0]) {
                        // Open the infowindow indicating the elevation at the clicked position.
                        //console.log(results[0].elevation)
                        $('#elevation').text(Math.round(results[0].elevation) + "m");
                    } else {
                        $('#elevation').text('No results found');
                    }
                } else {
                    $('#elevation').text('Elevation service failed due to: ' + status);
                }
            });

        }



        // initialize
        function Init() {

            initGoogle();
            getElevation();

        }

        // call initialization file
        if (window.File && window.FileList && window.FileReader) {
            Init();
        }

