// values of each item on the graph
xhours = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24];
xdays = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
xmonths = [1,2,3,4,5,6,7,8,9,10,11,12];
ykm = [1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7];

function init() {
	// intialize values for each variables
    hours = 24;
    days = 31;
    months = 12;
	Val_Max = 10;
	var stepSize = 1;
	var columnSize = 50;
	var rowSize = 60;
	var margin = 10;

    //DailyChart
    canvas = document.getElementById("dailyChart");
    context = canvas.getContext("2d");
    context.fillStyle = "#000;"
	yScale = (canvas.height - columnSize - margin) / (Val_Max);
    xScale = (canvas.width - rowSize) / (hours + 1);
    
	// column names 
	context.font = "16 pt Helvetica"
	var count =  0;
	for (scale=Val_Max;scale>=0;scale = scale - stepSize) {
		y = columnSize + (yScale * count * stepSize); 
		context.fillText(scale, 0,y + 5);
		context.moveTo(rowSize,y);
		count++;
	}
    context.stroke();
    
	// print names of each data entry
	context.font = "20 pt Verdana";
	context.textBaseline="bottom";
	for (i=0;i<24;i=i+2) {
		computeHeight(ykm[i]);
		context.fillText(xhours[i], xScale * (i+1),300);
	}
 	context.fillStyle="green";

	// translate to bottom of graph to match the data 
    context.translate(0,canvas.height - margin);
	context.scale(xScale,-1 * yScale);
  
	// draw each graph bars	
	for (i=0;i<24;i++) {
		context.fillRect(i+1, 0, 0.5, ykm[i]);
    }

    //MonthlyChart
    canvas2 = document.getElementById("monthlyChart");
    context2 = canvas2.getContext("2d");
    yScale = (canvas2.height - columnSize - margin) / (Val_Max);
    xScale = (canvas2.width - rowSize) / (31 + 1);
    context2.font = "16 pt Helvetica"
	var count =  0;
	for (scale=Val_Max;scale>=0;scale = scale - stepSize) {
		y = columnSize + (yScale * count * stepSize); 
		context2.fillText(scale, 0,y + 5);
		context2.moveTo(rowSize,y);
		count++;
	}
    context2.stroke();
    // print names of each data entry
    context2.font = "20 pt Verdana";
	context2.textBaseline="bottom";
	for (i=0;i<31;i=i+2) {
		computeHeight(ykm[i]);
		context2.fillText(xdays[i], xScale * (i+1),300);
    }
    context2.fillStyle="yellow";
    context2.translate(0,canvas2.height - margin);
	context2.scale(xScale,-1 * yScale);
  
	// draw each graph bars	
	for (i=0;i<31;i++) {
		context2.fillRect(i+1, 0, 0.25, ykm[i]);
    }

    //AnnualChart
    canvas3 = document.getElementById("annualChart");
    context3 = canvas3.getContext("2d");
    yScale = (canvas3.height - columnSize - margin) / (Val_Max);
    xScale = (canvas3.width - rowSize) / (months + 1);
    context3.font = "16 pt Helvetica"
	var count =  0;
	for (scale=Val_Max;scale>=0;scale = scale - stepSize) {
		y = columnSize + (yScale * count * stepSize); 
		context3.fillText(scale, 0,y + 5);
        context3.moveTo(rowSize,y);
        context.lineTo(canvas.width,y);
		count++;
	}
    context3.stroke();

    // print names of each data entry
    context3.font = "20 pt Verdana";
	context3.textBaseline="bottom";
	for (i=0;i<12;i++) {
		computeHeight(ykm[i]);
		context3.fillText(xmonths[i], xScale * (i+1),300);
    }
    context3.fillStyle="red";
    context3.translate(0,canvas3.height - margin);
	context3.scale(xScale,-1 * yScale);
  
	// draw each graph bars	
	for (i=0;i<12;i++) {
		context3.fillRect(i+1, 0, 0.3, ykm[i]);
    }
}

function computeHeight(value) {
	y = canvas.height - value * yScale ;	
}

init();
computeHeight();