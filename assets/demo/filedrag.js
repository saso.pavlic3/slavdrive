(


	function () {

		// getElementById
		function $id(id) {
			return document.getElementById(id);
		}


		// output information
		function Output(msg) {
			var m = $id("messages");
			m.innerHTML = msg + m.innerHTML;
		}


		// file drag hover
		function FileDragHover(e) {
			e.stopPropagation();
			e.preventDefault();
			e.target.className = (e.type == "dragover" ? "hover" : "");
		}


		// file selection
		function FileSelectHandler(e) {

			// cancel event and hover styling
			FileDragHover(e);

			// fetch FileList object
			var files = e.target.files || e.dataTransfer.files;

			// process all File objects
			for (var i = 0, f; f = files[i]; i++) {
				ParseFile(f);
			}

		}

/**
 * is used to output file information, save data to local storage,refresh dashboard, populate table with data.
 * 
 *  
 */
		
		function ParseFile(file) {

			Output(
				"<p>File information: <strong>" + file.name +
				"</strong> type: <strong>" + file.type +
				"</strong> size: <strong>" + file.size +
				"</strong> bytes</p>"
			);


			// display text
			if (file.type.indexOf("text") == 0) {
				
				var text;
				var lines;
				var reader = new FileReader();
				reader.onload = function (e) {

					text = e.target.result;
					var objects = make_objects(text);
					//displayObjects(objects);
					save(text);
					complexInitGoogleMaps();
					// Parsing lines into objects
					//TODO Luka you can store objects into IndexDB in browser

					var worker = new Worker('../assets/demo/workers.js');
					worker.addEventListener('message', function(e) {
						var data = e.data; 
						PopulateTableWithData(data);

					  }, false);


					worker.postMessage(text); // Send data to our worker.				

					

				}
				reader.readAsText(file);

			}

		}
/**
*Is used to populate the table with given data
*/
		function PopulateTableWithData(data) {
			
			var n = 0;
			if(data.length<100)
			{
				n = data.length;
			}else{
				n = 100;
			}

			for(x=0;x<n;x++){
		
				$('#dataTable > tbody:last-child').append("<tr> <td>" + (x+1) + "</td> <td>" + data[x].index + "</td> <td> " + data[x].timestamp + "</td> <td> " + data[x].latitude + " </td> <td>" + data[x].longtitude + " </td> </tr>");
			}
								
			$('<b>File has been succesfully parsed!</b><br>').appendTo('#messages');
			//https://maps.googleapis.com/maps/api/distancematrix/json?units=metrics&origins=39.90484,116.36838&destinations=39.93517,116.42706&key=AIzaSyBnsDxtIHJwyELYlLUsdMleSqrpflTQD2c
		}


		// initialize
		function Init() {

			var fileselect = $id("fileselect"),
				filedrag = $id("filedrag"),
				submitbutton = $id("submitbutton");

			// file select
			fileselect.addEventListener("change", FileSelectHandler, false);

			// is XHR2 available?
			var xhr = new XMLHttpRequest();
			if (xhr.upload) {

				// file drop
				filedrag.addEventListener("dragover", FileDragHover, false);
				filedrag.addEventListener("dragleave", FileDragHover, false);
				filedrag.addEventListener("drop", FileSelectHandler, false);
				filedrag.style.display = "block";

				
			}

		}

		// call initialization file
		if (window.File && window.FileList && window.FileReader) {
			Init();
		}


	})();