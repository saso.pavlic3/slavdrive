/**
 * This script is used to set the active button on the navigation 
 * bar when the user presses it.
 */

// Get the container element
var btnContainer = document.getElementById("sidebar-wrapper");

// Get all buttons with class="btn" inside the container
var btns = btnContainer.getElementsByClassName("button");

// Loop through the buttons and add the active class to the current/clicked button
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}