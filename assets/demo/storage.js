/** 
 * Takes the given text and converts it to objects
*/

function make_objects(text){
    var lines, objects = [];

    //Splits the text so that every line is one element
    lines = text.split("\n");

    //Split every line to get respective elements
    for (var i = 0; i < lines.length; i++){
        var temp = lines[i].split(",");
        var obj = {
            id : temp[0],
            date: temp[1],
            lng: temp[2],
            lat: temp[3],
        }

        //Add the new object to the array
        objects.push(obj);
    }
    return objects;
    
    /*
    i = 0;
    for(var i = 0;i < objects.length; i++){
        document.write(objects[i].id + ' ' + objects[i].date + ' '+ objects[i].lat + ' '+ objects[i].lng + '<br> ' );
    }
    */
}

/**
 * Takes the given data and stores it
*/

function save(text){
    //Parse the recieved text and store it
    var objects = make_objects(text);
    localStorage.objects = JSON.stringify(objects);

}

/** 
*Gets the data from storage and sends it for displaying
*/

function load(){
    var storedObjects = JSON.parse(localStorage.objects);
    displayObjects(storedObjects);

}

/**
*Is used to populate the table with given data
*/
function displayObjects(objects){
    
    //Get the reference to the table you want to populate
    var table_ref = document.getElementById('dataTable').getElementsByTagName('tbody')[0];

    //For each element, make a row and insert all it's data into cells
    for(var i = 0;i < objects.length; i++){
         var newRow = table_ref.insertRow();
         var newCell = newRow.insertCell(0);
         var newText = document.createTextNode(i+1);
         newCell.appendChild(newText);

         newCell = newRow.insertCell(1);
         newText = document.createTextNode(objects[i].id);
         newCell.appendChild(newText);

         newCell = newRow.insertCell(2);
         newText = document.createTextNode(objects[i].date);
         newCell.appendChild(newText);

         newCell = newRow.insertCell(3);
         newText = document.createTextNode(objects[i].lat);
         newCell.appendChild(newText);

         newCell = newRow.insertCell(4);
         newText = document.createTextNode(objects[i].lng);
         newCell.appendChild(newText);

        //document.write(objects[i].id + ' ' + objects[i].date + ' '+ objects[i].lat + ' '+ objects[i].lng + '<br> ' );
        
    }
}


/** 
*this method is for other programmers.
*return value is array of objects.
*every object is consisted of:
*    id
*    date
*    lng
*    lat
*
*/
function getObjects(){
   return JSON.parse(localStorage.objects);
}

