

function ParseText(text) {
    lines = text.split(/[\r\n]+/g);
    var points = []
    lines.forEach(element => {

        line = element.split(",")
        points.push(new Point(line[0], line[1], line[2], line[3]))
    });

    return points;
}

function PopulateTableWithData(data) {
    i = 0;
    data.forEach(x => {

        $('#dataTable > tbody:last-child').append("<tr> <td>" + i + "</td> <td>" + x.index + "</td> <td> " + x.timestamp + "</td> <td> " + x.latitude + " </td> <td>" + x.longtitude + " </td> </tr>");
        i++;
    })
}

self.addEventListener('message', function (e) {
    var data = e.data;   
    this.self.postMessage(ParseText(data))

}, false);


class Point {

    constructor(index, timestamp, latitude, longtitude) {
        this.index = index;
        this.timestamp = timestamp;
        this.latitude = latitude;
        this.longtitude = longtitude;
    }
}
