var latitude;
var longitude;
var map;

demo = {
  initDocumentationCharts: function () {
    if ($('#dailySalesChart').length != 0 && $('#websiteViewsChart').length != 0) {
      /* ----------==========     Daily Sales Chart initialization For Documentation    ==========---------- */


      optionsDailySalesChart = {
        lineSmooth: Chartist.Interpolation.cardinal({
          tension: 0
        }),
        low: 0,
        high: 400, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
        chartPadding: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        },
      }

      var dailySalesChart = new Chartist.Line('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);

      var animationHeaderChart = new Chartist.Line('#websiteViewsChart', dataDailySalesChart, optionsDailySalesChart);
    }
  },
  dragAndDrop: function () {
    $('.file-upload').file_upload();
  },


  initDashboardPageCharts: function () {

    if ($('#dailySalesChart').length != 0 || $('#completedTasksChart').length != 0 || $('#websiteViewsChart').length != 0) {
      /* ----------==========     Daily Sales Chart initialization    ==========---------- */



      optionsDailySalesChart = {
        lineSmooth: Chartist.Interpolation.cardinal({
          tension: 0
        }),
        low: 0,
        high: 50, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
        chartPadding: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        },
      }

      var dailySalesChart = new Chartist.Line('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);

      md.startAnimationForLineChart(dailySalesChart);



      /* ----------==========     Completed Tasks Chart initialization    ==========---------- */

      dataCompletedTasksChart = {
        labels: ['12p', '3p', '6p', '9p', '12p', '3a', '6a', '9a'],
        series: [
          [230, 750, 450, 300, 280, 240, 200, 190]
        ]
      };

      optionsCompletedTasksChart = {
        lineSmooth: Chartist.Interpolation.cardinal({
          tension: 0
        }),
        low: 0,
        high: 1000, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
        chartPadding: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      }

      var completedTasksChart = new Chartist.Line('#completedTasksChart', dataCompletedTasksChart, optionsCompletedTasksChart);

      // start animation for the Completed Tasks Chart - Line Chart
      md.startAnimationForLineChart(completedTasksChart);


      /* ----------==========     Emails Subscription Chart initialization    ==========---------- */

      var dataWebsiteViewsChart = {
        labels: ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D'],
        series: [
          [542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895]

        ]
      };
      var optionsWebsiteViewsChart = {
        axisX: {
          showGrid: false
        },
        low: 0,
        high: 1000,
        chartPadding: {
          top: 0,
          right: 5,
          bottom: 0,
          left: 0
        }
      };
      var responsiveOptions = [
        ['screen and (max-width: 640px)', {
          seriesBarDistance: 5,
          axisX: {
            labelInterpolationFnc: function (value) {
              return value[0];
            }
          }
        }]
      ];
      var websiteViewsChart = Chartist.Bar('#websiteViewsChart', dataWebsiteViewsChart, optionsWebsiteViewsChart, responsiveOptions);

      //start animation for the Emails Subscription Chart
      md.startAnimationForBarChart(websiteViewsChart);
    }
  },




/**
 * function is used to show the map on webpage,set map options.
 * return value: map object
 */
  initGoogleMaps: function () {
    var objects = getObjects();
    var start_object = objects[0];
    var start_coordinate = new google.maps.LatLng(Number(start_object.lat), Number(start_object.lng));

    //var myLatlng = new google.maps.LatLng(46.554650, 15.645881);
    var mapOptions = {
      zoom: 8,
      center: start_coordinate,
      scrollwheel: false, //we disable de scroll over the map, it is a really annoing when you scroll through page
      styles: [{
        "featureType": "water",
        "stylers": [{
          "saturation": 43
        }, {
          "lightness": -11
        }, {
          "hue": "#0088ff"
        }]
      }, {
        "featureType": "road",
        "elementType": "geometry.fill",
        "stylers": [{
          "hue": "#ff0000"
        }, {
          "saturation": -100
        }, {
          "lightness": 99
        }]
      }, {
        "featureType": "road",
        "elementType": "geometry.stroke",
        "stylers": [{
          "color": "#808080"
        }, {
          "lightness": 54
        }]
      }, {
        "featureType": "landscape.man_made",
        "elementType": "geometry.fill",
        "stylers": [{
          "color": "#ece2d9"
        }]
      }, {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [{
          "color": "#ccdca1"
        }]
      }, {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [{
          "color": "#767676"
        }]
      }, {
        "featureType": "road",
        "elementType": "labels.text.stroke",
        "stylers": [{
          "color": "#ffffff"
        }]
      }, {
        "featureType": "poi",
        "stylers": [{
          "visibility": "off"
        }]
      }, {
        "featureType": "landscape.natural",
        "elementType": "geometry.fill",
        "stylers": [{
          "visibility": "on"
        }, {
          "color": "#b8cb93"
        }]
      }, {
        "featureType": "poi.park",
        "stylers": [{
          "visibility": "on"
        }]
      }, {
        "featureType": "poi.sports_complex",
        "stylers": [{
          "visibility": "on"
        }]
      }, {
        "featureType": "poi.medical",
        "stylers": [{
          "visibility": "on"
        }]
      }, {
        "featureType": "poi.business",
        "stylers": [{
          "visibility": "simplified"
        }]
      }]

    };
    var map = new google.maps.Map(document.getElementById("map"), mapOptions);

    return map;
    
  },

/**gets map objects. Adds the very first and last coordinate as marker object to the map. 
 * Shows start coodrinate and end coordinate
 * @param {*} map 
 * 
 */
  addMarkersToMap: function (map){
    var objects = getObjects();
    var start_object = objects[0];
    var start_coordinate = new google.maps.LatLng(Number(start_object.lat), Number(start_object.lng));

    var end_object = objects[objects.length - 1];
    var end_coordinate = new google.maps.LatLng(Number(end_object.lat), Number(end_object.lng));

    var start_marker = new google.maps.Marker({
      position: start_coordinate,
      title: 'start'
    })
    var end_marker = new google.maps.Marker({
      position: end_coordinate,
      title: 'end'
    })
    start_marker.setMap(map);
    end_marker.setMap(map);
  }
}

/**
 * called at the very begging in order to calculate data from local storage and show it on dashboard,
 * initialize map and first and last coordinate to the map
 */

function complexInitGoogleMaps(){
  initGoogle();
  map = demo.initGoogleMaps();     
  demo.addMarkersToMap(map);

  findMe();
}

/**
 * alows dropping by preventing default link dropping
 * 
 */
function allowDrop(ev) {
  ev.preventDefault();
}
/**
 * enables dragging
 * @param {} ev- caught event  
 */
function drag(ev) {
  ev.dataTransfer.setData("text", ev.target.id);
}
/**
 * enables dropping 
 * @param {} ev- caught event  
 */
function drop(ev) {
  console.log('File(s) dropped');
  ev.stopPropagation();
  ev.preventDefault();
  const dt = ev.dataTransfer;
  const files = dt.files;


}



function findMe() {

  function success(position) {
    latitude  = position.coords.latitude;
    longitude = position.coords.longitude;
    document.getElementById('user_location').innerHTML = 'your current location: lat: ' + latitude + ' lng: ' + longitude;

  }

  function error() {
    status.textContent = 'Unable to retrieve your location';
  }

  if (!navigator.geolocation) {
    status.textContent = 'Geolocation is not supported by your browser';
  } else {
    status.textContent = 'Locating…';
    navigator.geolocation.getCurrentPosition(success, error);
  }

}

function showMeMyLocation(){
  var loc = new google.maps.LatLng(Number(latitude), Number(longitude));
  map.setCenter(loc);
  var loc_marker = new google.maps.Marker({
    position: loc,
    title: 'current location'
  })
  loc_marker.setMap(map);
  demo.addMarkersToMap(map);

}

$("#btn1").click(function () {
  alert("dela");
}); 