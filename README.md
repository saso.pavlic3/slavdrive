**Before you start using files**
* Download & Install XAMPP server
* Install Git Bash and generate SSH key, which you need to link in GitLab settings
* Pull GitLab project into server (see guidlines for instalation)
* Access files from http://127.0.0.1/*{path-to-pulled-files}